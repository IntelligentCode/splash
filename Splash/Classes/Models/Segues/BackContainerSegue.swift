//
//  BackContainerSegue.swift
//  Splash
//
//  Created by Артём Шляхтин on 07/03/2017.
//  Copyright © 2017 Artem Shlyakhtin. All rights reserved.
//

import UIKit

public class BackContainerSegue: UIStoryboardSegue {
 
    override init(identifier: String?, source: UIViewController, destination: UIViewController) {
        super.init(identifier: identifier, source: source, destination: destination)
        
        guard let splash = source as? SplashController else {
            fatalError("This type of segue must only be used from a SplashContoller")
        }
        
        splash.embed(controller: destination)
    }
    
    override public func perform() {
    }
    
}
