//
//  TopContainerSegue.swift
//  Splash
//
//  Created by Артём Шляхтин on 07/03/2017.
//  Copyright © 2017 Artem Shlyakhtin. All rights reserved.
//

import UIKit
import System

public class TopContainerSegue: UIStoryboardSegue {
    
    public var animates = false
    
    override init(identifier: String?, source: UIViewController, destination: UIViewController) {
        super.init(identifier: identifier, source: source, destination: destination)
        if isAllowPerform == false {
            splash?.addModalViewController(destination)
        }
    }
    
    override public func perform() {
        if animates == false {
            splash?.takeSnapshot(destination)
        }
        
        if isAllowPerform == false { return }
        topViewController?.present(destination, animated: animates, completion: nil)
    }
    
    // MARK: - Properties
    
    fileprivate var splash: SplashController? {
        get { return source as? SplashController }
    }
    
    fileprivate var isAllowPerform: Bool {
        get {
            guard let state = topViewController?.view.window?.isHidden else { return false }
            return !state
        }
    }
    
    fileprivate var topViewController: UIViewController? {
        get { return source.topViewController }
    }
    
}
