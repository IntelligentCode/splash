    //
//  SplashController.swift
//  Splash
//
//  Created by Артём Шляхтин on 07/03/2017.
//  Copyright © 2017 Artem Shlyakhtin. All rights reserved.
//

import UIKit
import System

open class SplashController: UIViewController {
    
    fileprivate var modalControllers = [UIViewController]()
    fileprivate var snapshot: UIView?
    
    // MARK: - Lifecycle

    override open func viewDidLoad() {
        super.viewDidLoad()
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        removeSnapshotIfNeeded()
        registerNotification()
    }
    
    open override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    open override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unregisterNotification()
    }

    override open func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation

    override open func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }
    
    // MARK: - Actions

    func addModalViewController(_ controller: UIViewController) {
        modalControllers.append(controller)
    }
    
    func embed(controller: UIViewController) {
        if childViewControllers.count > 0 { return }
        
        self.addChildViewController(controller)
        controller.view.frame = self.view.bounds
        self.view.insertSubview(controller.view, at: 0)
        controller.didMove(toParentViewController: self)
    }
    
    func takeSnapshot(_ controller: UIViewController) {
        snapshot?.removeFromSuperview()
        snapshot = controller.view.snapshotView(afterScreenUpdates: true)
        if let screen = snapshot {
            controller.beginAppearanceTransition(false, animated: false)
            self.view.addSubview(screen)
            controller.endAppearanceTransition()
        }
    }
    
    private func removeSnapshotIfNeeded() {
        if modalControllers.count > 0 { return }
        snapshot?.removeFromSuperview()
        snapshot = nil
    }
    
    // MARK: - Notifications
    
    func windowDidBecomeVisible(_ notification: NSNotification) {
        //TODO: сделать проверку на view. От той ли вьюхи пришел нотификатион.
        for controller in modalControllers {
            self.beginAppearanceTransition(false, animated: false)
            topViewController?.present(controller, animated: false, completion: nil)
            self.endAppearanceTransition()
        }
        modalControllers.removeAll()
    }
    
    fileprivate func registerNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(SplashController.windowDidBecomeVisible), name: .UIWindowDidBecomeVisible, object: nil)
    }
    
    fileprivate func unregisterNotification() {
        NotificationCenter.default.removeObserver(self, name: .UIWindowDidBecomeVisible, object: nil)
    }

}
