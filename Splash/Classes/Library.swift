//
//  Library.swift
//  Splash
//
//  Created by Артём Шляхтин on 07/03/2017.
//  Copyright © 2017 Artem Shlyakhtin. All rights reserved.
//

import Foundation

public struct SplashFramework {
    public static let bundleName = "ru.roseurobank.Splash"
}
